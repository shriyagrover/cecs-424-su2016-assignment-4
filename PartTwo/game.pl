/*Refreshment Day.
Command "start" to have a fun trippy day. */
:- i_am_at/1, dynamic at/2.

path(start, n, zoo_visit).
path(start, nw, fun_activities).
path(start, ne, recreation_activities).

path(zoo_visit, w, fun_activities) :- ticket(full_park).
path(zoo_visit, e, recreation_activities) :- ticket(full_park).

start:-
write('WELCOME TO THE THEME PARK'),
write('Get your suitable ticket'),
write('Read the manual for how to start'),
manual.

/*From which counters to buy the tickets of choice*/
at(A, fun_activities).
at(B, zoo_visit).
at(C, recreation_activities).
at(D, full_park).

at(equipments, A).
at(towels, C).
at(equipments, towels, D).

buy(X) :-
i_am_at(place),
at(X, place),
take(ticket),
write('Now look for directions manual').

buy(_) :-
write('You don''t have your ticket pass to enter the park').


manual_for_buying_the_tickets :-
ticket(fun_activities, $80),
ticket(zoo_visit, $60),
ticket(recreation_activities, $90),
ticket(full_park, $200).

manual_for_directions :-
write('You can only move in any of the logical north directions').

/* These rules define the three directions where we can go. */
n :- walk(n).
nw :- walk(nw).
ne :- walk(ne).

/* This rule tells how to move in a given direction. */

location(_) :-
write('Fenced area').

location(you, entrance).

goto(X) :- 
location(you, L),
connect(L, X),
retract(location(you, L)),
assert(location(you,X)),
write('You are'), write(X), nl.

take(X) :-
		i_am_at(place),
		at(X, place),
		retract(at(X, place)),
		assert(at(X.in_hand)),
		write('Okay'),
		nl.

take(X) :-
		at(X, in_hand),
		write('You already have that'),
		nl.

take(_) :-
		write('oops i forgot the things to take with me'),
		nl.

drop(X) :-
        at(X, in_hand),
        i_am_at(Place),
        retract(at(X, in_hand)),
        assert(at(X, Place)),
        write('Thank you for returning'),
        nl.

drop(_) :-
        write('You don''t have anything'),
        nl.


describe(fun_activities) :-
write('Welcome to the fun activity area'), nl,
write('Here you can do horse riding, shooting, archery'), nl,
write('We hope you have a great refreshing day'), nl,
day_end.

describe(zoo_visit) :-
write('The safari will take you to the exotic location where you can experience different variety of birds and reptiles'), nl,
write('Hoping you have a adventorous day ahead'), nl,
day_end.

describe(recreation_activities) :-
write('You can experience a relaxing therapy with head and hair spas and as well as body massages'), nl,
write('Wishing for you a relaxing day ahead'), nl,
day_end.

day_end :-
write('Thank you for your visit. Hoping to see you again and giving us a chance to serve you again'),
nl.

manual :-
write('Read the mauls for buying the tickets and as well as for directions to proceed further'), nl,
manual_for_directions,
manual_for_buying_the_tickets.


