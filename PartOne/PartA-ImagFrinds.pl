contestant(Joanne).
contestant(Lou).
contestant(Ralph).
contestant(Winnie).

animal(Grizzly_bear).
animal(Moose).
animal(Seal).
animal(Zebra).

adventure(Circus).
adventure(Rock_band).
adventure(Spaceship).
adventure(Train).

solve :-

animal(JoanneCharacter), animal(LouCharacter), animal(RalphCharacter), animal(WinnieCharacter).
all_different([JoanneCharacter, LouCharacter, RalphCharacter, WinnieCharacter]),

adventure(JoanneCharAdven), adventure(LouCharAdevn), adventure(RalphCharAdven), adventure(WinnieCharAdven).
all_different([JoanneCharAdven, LouCharAdevn, RalphCharAdven, WinnieCharAdven]),


Triples = [ [Joanne, JoanneCharacter, JoanneCharAdven],
            [Lou, LouCharacter. LouCharAdevn],
            [Ralph, RalphCharacter, RalphCharAdven],
            [Winnie, WinnieCharacter, WinnieCharAdven] ],

% 1. seal neither Joanne nor Lou nor Spaceship nor Train.
( (member([Joanne, _, Spaceship], Triples),
	member([Lou, _, Train], Triples));

 (member([Joanne, _, Spaceship], Triples),
	member([Lou, _, Train], Triples)) ),


% 2. Joanne not Grizzly_bear but went to circus.
\+ member([Joanne, Grizzly_bear, circus], Triples),


% 3. Winnie character is Zebra
\+ member([Winnie, Zebra, _], Triples),

% 4.Grizzly_bear didnt borad the Spaceship
\+ member([_, Grizzly_bear, Spaceship], Triples),



Tell(Joanne, JoanneCharacter, JoanneCharAdven),
Tell(Lou, LouCharacter. LouCharAdevn),
Tell(Ralph, RalphCharacter, RalphCharAdven),
Tell(Winnie, WinnieCharacter, WinnieCharAdven).

all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z) :-
write('Young author named '), write(X), write(' has imaginary friend as '), write(Y),
write(' specifies in '), write(Z), write('.'), nl.